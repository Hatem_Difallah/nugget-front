import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../../models/User';

@Component({
  selector: 'app-list-cv',
  templateUrl: './list-cv.component.html',
  styleUrls: ['./list-cv.component.css']
})
export class ListCvComponent implements OnInit {

  // variables
  @Input() users: User[];
  @Output() userSelected = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
    if (this.users.length > 0) {
      this.userSelected.emit(this.users[0]);
    }
  }

  // getSelectedUser
  getSelectedUser(userSelected) {
    this.userSelected.emit(userSelected);
  }
}
