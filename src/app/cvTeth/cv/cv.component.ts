import {Component, OnInit} from '@angular/core';
import {User} from '../../../models/User';
import {UserService} from '../../../http/user.service';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css']
})
export class CvComponent implements OnInit {


  // variables
  users: any = [];
  userSelected: User;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.buildUsers();
  }

  // getUserSelected
  getUserSelected(user) {
    this.userSelected = user;
  }

  // buildUsers
  buildUsers() {
     return this.userService.getUsers().subscribe((data) => {
      this.users = data;
      this.userSelected = this.users[0];
     });
  }
}
