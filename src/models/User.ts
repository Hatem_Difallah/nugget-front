export class User {

  id: number;
  firstName: string;
  lastName: string;
  imageUrl: string;
  job: string;
  description: string;

  constructor(id = 0, firstName = '', lastName = '', imageUrl = '',
              job = '', description = '') {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.imageUrl = imageUrl;
    this.job = job;
    this.description = description;
  }
}
