import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../../models/User';

@Component({
  selector: 'app-item-cv',
  templateUrl: './item-cv.component.html',
  styleUrls: ['./item-cv.component.css']
})
export class ItemCvComponent implements OnInit {

  // variables
  @Input() user: User;
  @Output() selectedUser = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  // getCvDetail
  getCvDetail() {
    this.selectedUser.emit(this.user);
  }
}
