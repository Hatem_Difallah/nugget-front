import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {User} from '../models/User';
import {Injectable} from '@angular/core';

@Injectable()
export class UserService {

  // Define API
  apiURL = 'http://localhost:8085/micro-service-user/';

  // list users
  users: any[];

  constructor(private http: HttpClient) {
  }

  /*========================================
   Methods for consuming RESTful API
  =========================================*/

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // Fetch users list
  getUsers(): Observable<User> {
    return this.http.get<User>(this.apiURL + '/user/users')
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
