import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {CvComponent} from './cvTeth/cv/cv.component';
import {ListCvComponent} from './cvTeth/list-cv/list-cv.component';
import {ItemCvComponent} from './cvTeth/item-cv/item-cv.component';
import {DetailCvComponent} from './cvTeth/detail-cv/detail-cv.component';
import {HttpClientModule} from '@angular/common/http';
import {UserService} from '../http/user.service';


@NgModule({
  declarations: [
    AppComponent,
    CvComponent,
    ListCvComponent,
    ItemCvComponent,
    DetailCvComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
